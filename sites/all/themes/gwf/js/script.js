(function($, Drupal, window, document, undefined) {

    // To understand behaviors, see https://drupal.org/node/756722#behaviors
    Drupal.behaviors.my_custom_behavior = {
        attach: function(context, settings) {


    var plang = $('html').attr('lang');

    var messages_default = {s_unspecified:"Unspecified", s_sresult:"Saving result", s_sresultw:"Saving results with", s_compare: 'Compare'};

    if(plang == 'ru'){
        messages_default = {s_unspecified:"Unspecified", s_sresult:"Результат экономии", s_sresultw:"Результат экономии c", s_compare: 'Сравнимаем'};
    }

    if($('body').hasClass('section-dealer')){
        setTimeout(function() { window.location.reload() }, 300000);
    }

    $('#images-slides').cycle({
        fx: 'fade',
        speed: 2666,
        after: onAfter,
        pager:  '#nav', 
        delay: 1000,
        
        pagerAnchorBuilder: function(idx, slide) { 
            return '<div class="thumbnail-item"><a href="#"><img src="' + slide.src + '" width="35" height="35" /></a></div>'; 
        } 
    });

    function onAfter(curr, next, opts) {
        var index = opts.currSlide;
    }

    //
    //  Economy form calculation
    //

    $("#map_to_show_button").click(function() {
        $('#map_to_show').dialog({
            width: 500
        });
    });
    


    $('#edit-windows-type-1').change(function() {
        var value = $(this).val();
        
        var printvalue = value;
        
        if(value == "[0]"){
            printvalue = messages_default.s_unspecified;
        }
        
        $('#first_window').text(printvalue);
 
        var matches_one = value.match(/\[(.*?)\]/);
        var submatch_one = matches_one[1];
        window.myWindow1 = submatch_one;
        window.myWindow1_name = printvalue;
        $('#edit-calc1-window').val(submatch_one);
    });

    $('#edit-windows-type-2').change(function() {
        var value = $(this).val();
        
        var printvalue2 = value;
        
        if(value == "[0]"){
            printvalue2 = messages_default.s_unspecified;
        }
        
        $('#second_window').text(printvalue2);
  
        var matches_two = value.match(/\[(.*?)\]/);
        var submatch_two = matches_two[1];
        window.myWindow2 = submatch_two;
        window.myWindow2_name = printvalue2;
        $('#edit-calc2-window').val(submatch_two);
    });
    
    $('#edit-windows-type-2, #edit-windows-type-1').change(function() {
        var calc_value1 = window.myWindow1
        var calc_value2 = window.myWindow2
        
        if( calc_value1 < 0.34 && calc_value2 <= 0.34){
            $('h2.third').html(messages_default.s_sresult);
        }else{
            if($("#first_window").length == 0){

                $('h2.third').html(messages_default.s_compare+'“<span id="first_window"></span>”<br>c “<span id="second_window"></span>”');
            }
            
            if(calc_value1 > calc_value2){
                var value = window.myWindow1_name;        
                $('h2.third').html(messages_default.s_sresultw+'"<span id="eco_product">'+value+'</span>"<br/>от "Вікна ГВФ"');
            }else{
                var value2 = window.myWindow2_name; 
                $('h2.third').html(messages_default.s_sresultw+'"<span id="eco_product">'+value2+'</span>"<br/>от "Вікна ГВФ"');
            }
        }
        
    });

    //    $('#edit-fasad-lenght, #edit-fasad-wight, #edit-fasad-height, #edit-glass-percent, #edit-type').change(function() {
    $('#custom-form-element-demo-form').change(function() {
        var f_lenght = $('#edit-fasad-lenght').val();
        var f_wight = $('#edit-fasad-wight').val();
        var f_height = $('#edit-fasad-height').val();
        var f_percent = $('#edit-glass-percent').val();
        var roof_basement = $('#edit-type').val();
        $('#edit-floor-space').val(f_lenght*f_wight);
        $('#edit-windows-doors').val(f_lenght*f_wight*f_percent);
        $('#edit-roof-space, #edit-basement').val(f_lenght*f_wight*roof_basement);
    
        if(roof_basement == "1"){
            $('#edit-walls').val(2*f_lenght*f_height+2*f_wight*f_height);
        }else{
            $('#edit-walls').val((2*f_lenght*f_height+2*f_wight*f_height)/2);
        }
    });
    
    //
    //  Resistance Block#1
    //    

    //    $('#edit-walls, #edit-calc1-walls, #edit-windows-doors, #edit-temp-outdoor, #edit-temp-indoor, #edit-calc1-walls, #edit-calc1-window, #edit-windows-doors, #edit-windows-type-1').change(function() {
    $('#custom-form-element-demo-form').change(function() {
    
        var d_temp = $('#edit-temp-indoor').val()-$('#edit-temp-outdoor').val();

        var walls = $('#edit-walls').val();
        var calc1_walls = $('#edit-calc1-walls').val();

   
        var basement = $('#edit-basement').val();
        var calc1_basement = $('#edit-calc1-basement').val();       
       
       
        var roof_space = $('#edit-roof-space').val();
        var calc1_roof_space = $('#edit-calc1-roof-space').val();           
           
           
        var windows_doors = $('#edit-windows-doors').val();
        var calc1_window = $('#edit-calc1-window').val(); 
        //
        //        Teplo property
        //   
        var teplo_potery1 = d_temp/(calc1_walls/walls);
        $('#teplo-potery1').html(teplo_potery1.toFixed(2));
        
        var teplo_potery2 = d_temp/(calc1_basement/basement);
        $('#teplo-potery2').html(teplo_potery2.toFixed(2));
        
        var teplo_potery3 = d_temp/(calc1_roof_space/roof_space);
        $('#teplo-potery3').html(teplo_potery3.toFixed(2));
        
        var teplo_potery4 = d_temp/(calc1_window/windows_doors);
        $('#teplo-potery4').html(teplo_potery4.toFixed(2));
   
        var teplo_sum = teplo_potery1+teplo_potery2+teplo_potery3+teplo_potery4;
        $('#sum_result1').html(teplo_sum.toFixed(2));
        //
        //        Percent property
        //      
        
        var percent_potery1 = (teplo_potery1*100)/teplo_sum;
        $('#percent-potery1 .value').text(percent_potery1.toFixed(2));
        
        var percent_potery2 = (teplo_potery2*100)/teplo_sum;
        $('#percent-potery2 .value').text(percent_potery2.toFixed(2));
        
        var percent_potery3 = (teplo_potery3*100)/teplo_sum;
        $('#percent-potery3 .value').text(percent_potery3.toFixed(2));
        
        var percent_potery4 = (teplo_potery4*100)/teplo_sum;
        $('#percent-potery4 .value').text(percent_potery4.toFixed(2));
    
    
        
        var sum_percent = percent_potery1+percent_potery2+percent_potery3+percent_potery4;
        $('#sum_percent  .value').html(sum_percent.toFixed(0));
    
        //    (walls+basement+roof_space+windows_doors)/
           
        var total_soprotivl = parseFloat(walls)+parseFloat(basement)+parseFloat(roof_space)+parseFloat(windows_doors);
        var result_soprotiv = total_soprotivl/((walls/calc1_walls)+(basement/calc1_basement)+(roof_space/calc1_roof_space)+(windows_doors/calc1_window));
        
        $('#total_soprotivl').html(result_soprotiv.toFixed(2));
        
      
        // ------------------  Heating by electro --------------------
        
        var loss_heat_day1 = teplo_sum*24/1000;
        $('#loss_heat_day1 .value').html(loss_heat_day1.toFixed(2));
        
        var loss_heat_month1 = loss_heat_day1*31;
        $('#loss_heat_month1 .value').html(loss_heat_month1.toFixed(2));
        
        var loss_heat_cost_day1 = loss_heat_day1*0.34;
        $('#loss_heat_cost_day1 .value').html(loss_heat_cost_day1.toFixed(2));

        var loss_heat_cost_month1 = 150*0.34+(400-150)*0.39+(loss_heat_month1-400)*0.92;
        $('#loss_heat_cost_month1 .value').html(loss_heat_cost_month1.toFixed(2));
       
        // ------------------  Heating by gas --------------------  
       
        var loss_gaz_heat_day1 = loss_heat_day1*0.1;
        $('#loss_gaz_heat_day1 .value').html(loss_gaz_heat_day1.toFixed(2));
        
        var loss_gaz_heat_month1 = loss_heat_month1*0.1;
        $('#loss_gaz_heat_month1 .value').html(loss_gaz_heat_month1.toFixed(2));
       
        var loss_gaz_heat_cost_day1 = loss_gaz_heat_day1*2.08;
        $('#loss_gaz_heat_cost_day1 .value').html(loss_gaz_heat_cost_day1.toFixed(2));

        var loss_gaz_heat_cost_month1 = loss_gaz_heat_month1*2.08;
        $('#loss_gaz_heat_cost_month1 .value').html(loss_gaz_heat_cost_month1.toFixed(2));
       
    });
    //
    //  Resistance Block#2
    //
    $('#custom-form-element-demo-form').change(function() {
    
        var d_temp = $('#edit-temp-indoor').val()-$('#edit-temp-outdoor').val();

        var walls = $('#edit-walls').val();
        var calc2_walls = $('#edit-calc2-walls').val();

   
        var basement = $('#edit-basement').val();
        var calc2_basement = $('#edit-calc2-basement').val();       
       
       
        var roof_space = $('#edit-roof-space').val();
        var calc2_roof_space = $('#edit-calc2-roof-space').val();           
           
        var windows_doors = $('#edit-windows-doors').val();   
        var calc2_window = $('#edit-calc2-window').val(); 

        //
        //        Teplo property
        //   
        
        var teplo2_potery1 = d_temp/(calc2_walls/walls);
        $('#teplo2-potery1').html(teplo2_potery1.toFixed(2));
        
        var teplo2_potery2 = d_temp/(calc2_basement/basement);
        $('#teplo2-potery2').html(teplo2_potery2.toFixed(2));
        
        var teplo2_potery3 = d_temp/(calc2_roof_space/roof_space);
        $('#teplo2-potery3').html(teplo2_potery3.toFixed(2));
        
        var teplo2_potery4 = d_temp/(calc2_window/windows_doors);
        $('#teplo2-potery4').html(teplo2_potery4.toFixed(2));
   
        var teplo2_sum = teplo2_potery1+teplo2_potery2+teplo2_potery3+teplo2_potery4;
        $('#sum2_result1').html(teplo2_sum.toFixed(2));
        //
        //        Percent property
        //      
        
        var percent2_potery1 = (teplo2_potery1*100)/teplo2_sum;
        $('#percent2-potery1 .value').text(percent2_potery1.toFixed(2));
        
        var percent2_potery2 = (teplo2_potery2*100)/teplo2_sum;
        $('#percent2-potery2 .value').text(percent2_potery2.toFixed(2));
        
        var percent2_potery3 = (teplo2_potery3*100)/teplo2_sum;
        $('#percent2-potery3 .value').text(percent2_potery3.toFixed(2));
        
        var percent2_potery4 = (teplo2_potery4*100)/teplo2_sum;
        $('#percent2-potery4 .value').text(percent2_potery4.toFixed(2));
    
    
        
        var sum2_percent = percent2_potery1+percent2_potery2+percent2_potery3+percent2_potery4;
        $('#sum2_percent .value').html(sum2_percent.toFixed(0));
    
        //    (walls+basement+roof_space+windows_doors)/
           
        var total_soprotiv2 = parseFloat(walls)+parseFloat(basement)+parseFloat(roof_space)+parseFloat(windows_doors);
        var result_soprotiv2 = total_soprotiv2/((walls/calc2_walls)+(basement/calc2_basement)+(roof_space/calc2_roof_space)+(windows_doors/calc2_window));
        
        $('#total_soprotiv2').html(result_soprotiv2.toFixed(2));
        
        
        // ------------------  Heating by electro --------------------
        
        var loss_heat_day2 = teplo2_sum*24/1000;
        $('#loss_heat_day2 .value').html(loss_heat_day2.toFixed(2));
        
        var loss_heat_month2 = loss_heat_day2*31;
        $('#loss_heat_month2 .value').html(loss_heat_month2.toFixed(2));
        
        var loss_heat_cost_day2 = loss_heat_day2*0.34;
        $('#loss_heat_cost_day2 .value').html(loss_heat_cost_day2.toFixed(2));

        var loss_heat_cost_month2 = 150*0.34+(400-150)*0.39+(loss_heat_month2-400)*0.92;
        $('#loss_heat_cost_month2 .value').html(loss_heat_cost_month2.toFixed(2));
       
        // ------------------  Heating by gas --------------------  
       
        var loss_gaz_heat_day2 = loss_heat_day2*0.1;
        $('#loss_gaz_heat_day2 .value').html(loss_gaz_heat_day2.toFixed(2));
        
        var loss_gaz_heat_month2 = loss_heat_month2*0.1;
        $('#loss_gaz_heat_month2 .value').html(loss_gaz_heat_month2.toFixed(2));
       
        var loss_gaz_heat_cost_day2 = loss_gaz_heat_day2*2.08;
        $('#loss_gaz_heat_cost_day2 .value').html(loss_gaz_heat_cost_day2.toFixed(2));

        var loss_gaz_heat_cost_month2 = loss_gaz_heat_month2*2.08;
        $('#loss_gaz_heat_cost_month2 .value').html(loss_gaz_heat_cost_month2.toFixed(2));
        
    });
    
    //    
    //  Check for correct input value 
    //  @outdoor temperature
    //  @indoor temperature
    //
      
    $('#edit-temp-outdoor').change(function() {
        var value = $('#edit-temp-outdoor').val();
        var max = -31;
        var min = -4;
    
        if ( max<value && value < min){
            
        }else{
            alert('Недопустимое значение');
            $('#edit-temp-outdoor').val('-20');
        }     
        
    });

    $('#edit-temp-indoor').change(function() {
        var value = $('#edit-temp-indoor').val();
        var max = 26;
        var min = 14;

        if (value>min && value<max){
           
        }else{
            alert('Недопустимое значение');
            $('#edit-temp-indoor').val('20');
        }
        
    });
    
    //    
    //  Create graph
    //  Calculate percent value of economy, and description for economy by 1,5,15,25 years
    //    

    setInterval(function() { 
    
        var loss_heat_cost_month1 = parseFloat($('#loss_heat_cost_month1 .value').html());
        var loss_heat_cost_month2 = parseFloat($('#loss_heat_cost_month2 .value').html());

        var tottal_ecomom2; // define economy by electricity
        var percent_tottal_ecomom2; // value for percent calculation
        
        if(loss_heat_cost_month1>loss_heat_cost_month2){
            tottal_ecomom2 = loss_heat_cost_month1-loss_heat_cost_month2;
            percent_tottal_ecomom2 = loss_heat_cost_month1;
        }
        else{
            tottal_ecomom2 = loss_heat_cost_month2-loss_heat_cost_month1;
            percent_tottal_ecomom2 = loss_heat_cost_month2;
        }
                
        $('#elektr_ecomom_month').html(tottal_ecomom2.toFixed(2));
        var elektr_ecomom_year = tottal_ecomom2*6;
        $('#elektr_ecomom_year').html(elektr_ecomom_year.toFixed(2));
        var elektr_ecomom_5years = tottal_ecomom2*6*5;
        $('#elektr_ecomom_5years').html(elektr_ecomom_5years.toFixed(2));
        var elektr_ecomom_15years = tottal_ecomom2*6*15;
        $('#elektr_ecomom_15years').html(elektr_ecomom_15years.toFixed(2));
        var elektr_ecomom_25years = tottal_ecomom2*6*25;
        $('#elektr_ecomom_25years').html(elektr_ecomom_25years.toFixed(2));
        

        var loss_gaz_heat_cost_month1 = parseFloat($('#loss_gaz_heat_cost_month1 .value').html());
        var loss_gaz_heat_cost_month2 = parseFloat($('#loss_gaz_heat_cost_month2 .value').html());
        //console.log(loss_gaz_heat_cost_month1);
        var tottal_gaz_ecomom2; // define economy by gas
        var percent_tottal_gaz_ecomom2; // value for percent calculation
        
        if(loss_gaz_heat_cost_month1>loss_gaz_heat_cost_month2){
            tottal_gaz_ecomom2 = loss_gaz_heat_cost_month1-loss_gaz_heat_cost_month2;
            percent_tottal_gaz_ecomom2 = loss_gaz_heat_cost_month1;
        }else{
            tottal_gaz_ecomom2 = loss_gaz_heat_cost_month2-loss_gaz_heat_cost_month1;
            percent_tottal_gaz_ecomom2 = loss_gaz_heat_cost_month2;
        }
        
        $('#gaz_ecomom_month').html(tottal_gaz_ecomom2.toFixed(2));
        var gaz_ecomom_year = tottal_gaz_ecomom2*6;
        $('#gaz_ecomom_year').html(gaz_ecomom_year.toFixed(2));
        var gaz_ecomom_5years = tottal_gaz_ecomom2*6*5;
        $('#gaz_ecomom_5years').html(gaz_ecomom_5years.toFixed(2));
        var gaz_ecomom_15years = tottal_gaz_ecomom2*6*15;
        $('#gaz_ecomom_15years').html(gaz_ecomom_15years.toFixed(2));
        var gaz_ecomom_25years = tottal_gaz_ecomom2*6*25;
        $('#gaz_ecomom_25years').html(gaz_ecomom_25years.toFixed(2));

        var comb_ecomom_month = tottal_gaz_ecomom2+(tottal_ecomom2/2*0.2);
        $('#comb_ecomom_month').html(comb_ecomom_month.toFixed(2));
        var comb_ecomom_year = comb_ecomom_month*6;
        $('#comb_ecomom_year').html(comb_ecomom_year.toFixed(2));
        var comb_ecomom_5years = comb_ecomom_month*6*5;
        $('#comb_ecomom_5years').html(comb_ecomom_5years.toFixed(2));
        var comb_ecomom_15years = comb_ecomom_month*6*15;
        $('#comb_ecomom_15years').html(comb_ecomom_15years.toFixed(2));
        var comb_ecomom_25years = comb_ecomom_month*6*25;
        $('#comb_ecomom_25years').html(comb_ecomom_25years.toFixed(2));
    
        // Set chart
        // @d1 - electro
        // @d2 - gas
        // @d3 - combined
        
        
        var d1 = [[1, elektr_ecomom_year], [5, elektr_ecomom_5years], [15, elektr_ecomom_15years], [25, elektr_ecomom_25years]];
        var d2 = [[1, gaz_ecomom_year], [5, gaz_ecomom_5years], [15, gaz_ecomom_15years], [25, gaz_ecomom_25years]];
        var d3 = [[1, comb_ecomom_year], [5, comb_ecomom_5years], [15, comb_ecomom_15years], [25, comb_ecomom_25years]];
        
        if($('#placeholder').length){
            $.plot("#placeholder", [ d1, d2, d3]);    
        }
        
        // 
        // Calculate percent
        //
        
        var gaz_ecomom_percent = tottal_gaz_ecomom2*100/percent_tottal_gaz_ecomom2;
        var elektr_ecomom_percent = tottal_ecomom2*100/percent_tottal_ecomom2;
        var comb_ecomom_percent = gaz_ecomom_percent+(elektr_ecomom_percent/2*0.2);
        $('#gaz_ecomom_percent').html(gaz_ecomom_percent.toFixed(2)+' %');
        $('#elektr_ecomom_percent').html(elektr_ecomom_percent.toFixed(2)+' %');
        $('#comb_ecomom_percent').html(comb_ecomom_percent.toFixed(2) +' %');

    }, 250);
        }
    };


})(jQuery, Drupal, this, this.document);