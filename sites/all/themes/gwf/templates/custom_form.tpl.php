<?php
/**
 * @file
 * Zen theme's implementation to display a block.
 *
 */

// add float js 
global $language;

?>
<p class="intro_text"><?php print t('Use our economy calculation form'); ?></p>

<h2 class="first title"><?php print t('Enter'); ?><br /><?php print t('base values'); ?><sup class="star">*</sup></h2>
<div class="region-basic-data">
    <div id="basic_data" class="form-block">
        <div class="value-box">
            <table class="enter-values">
                <tbody>
                    <tr class="header">
                        <td colspan="2" class="col1"><?php print t('Base features'); ?></td>
                        <td colspan="2" class="col2"><?php print t('Walling Area'); ?></td>
                    </tr>
                    <tr class="first">
                        <td class="text"><?php print t('Type:'); ?></td>
                        <td class="value"><?php print drupal_render($variables['form']['type']); ?></td>
                        <td class="text2"><?php print t('Place<br />area'); ?></td>
                        <td class="value2"><?php print drupal_render($variables['form']['floor_space']); print(t('m')); ?><sup>2</sup></td>
                    </tr>
                    <tr class="middle">
                        <td class="text"><?php print t('Frontage length'); ?></td>
                        <td><?php print drupal_render($variables['form']['fasad_lenght']);  print(t('m')); ?></td>
                        <td class="text2"><?php print t('Walls'); ?></td>
                        <td><?php print drupal_render($variables['form']['walls']);  print(t('m')); ?><sup>2</sup></td>
                    </tr>
                    <tr class="middle">
                        <td class="text"><?php print t('Facade length'); ?></td>
                        <td><?php print drupal_render($variables['form']['fasad_wight']);  print(t('m')); ?></td>
                        <td class="text2"><?php print t('Cellar'); ?></td>
                        <td><?php print drupal_render($variables['form']['basement']);  print(t('m')); ?><sup>2</sup></td>
                    </tr>
                    <tr class="middle">
                        <td class="text"><?php print t('Facade height'); ?></td>
                        <td><?php print drupal_render($variables['form']['fasad_height']);  print(t('m')); ?></td>
                        <td class="text2"><?php print t('Attic'); ?></td>
                        <td><?php print drupal_render($variables['form']['roof_space']);  print(t('m')); ?><sup>2</sup></td>
                    </tr>
                    <tr class="last">
                        <td class="text"><?php print t('Factor'); ?><sup class="star">*</sup><br /><?php print(t('glazing')); ?></td>
                        <td><?php print drupal_render($variables['form']['glass_percent']); ?></td>
                        <td class="text2"><?php print t('Door window'); ?></td>
                        <td><?php print drupal_render($variables['form']['windows-doors']);  print(t('m')); ?><sup>2</sup></td>
                    </tr>
                    <tr class="bottom">
                        <td><?php print(t('The outside temperature')); ?> </td>
                        <td><?php print drupal_render($variables['form']['temp_outdoor']); ?>t ºC</td>
                        <td><?php print(t('Room temperature')); ?></td>
                        <td><?php print drupal_render($variables['form']['temp_indoor']); ?>t ºC</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <p><sup class="star">*</sup><?php print t('In case if the width and length of the facade unknown, sample values can be used to calculate, based on the total space.'); ?></p>
        <p><sup class="star">*</sup><?php print t('Glazing factor in the calculations, set to average, and depending on the area of windows and balcony doors, as well as the total area of the premises.'); ?></p>
    </div>

    <div id="map" class="form-block map">
        <img id="map_full" title="<?php print t('Temperature zones map of Ukraine'); ?>" src="/sites/all/themes/gwf/images/map.png">
        <img id="map_to_show" class="hide" title="<?php print t('Temperature zones map of Ukraine'); ?>" src="/sites/all/themes/gwf/images/map2.png">
        <h2><?php print t('Temperature zones map of Ukraine'); ?></h2>
        <a id="map_to_show_button" href="javascript:void(0);" title="<?php print t('Show temperature zones map of Ukraine'); ?>"><?php print t('Open'); ?></a>
    </div>
</div>

<h2 class="calculate-data second title"><?php print t('Compare'); ?>   “<span id="first_window"></span>”<br /> <?php print t('with'); ?> “<span id="second_window"></span>”</h2>

<a class="glass-sravni" href="/?q=ru/glastroesch-list"><?php print t('Comparison of glass units'); ?></a>

<div class="region-calculate-data">
    <div id="compare_1" class="compare_1">
        <?php print drupal_render($variables['form']['windows_type_1']); ?>
        <div class="value-box">
            <div class="enter-value">
                <table>
                    <tbody>
                        <tr class="t_header">
                            <th class="one" colspan="3"><?php print t('Normative value of heat transfer resistance<br />according to DBN В.2.6-31:2006'); ?></th>
                            <th class="two" colspan="3"><?php print t('Heat loss through the building structure, <span>* excluding ventilation losses and air-conditioning'); ?></span></th>
                        </tr>
                        <tr class="datarow">
                            <td class="columl1" ><?php print(t('Walls')); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc1_walls']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo-potery1" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent-potery1"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td class="columl1" ><?php print(t('Cellar')); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc1_basement']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo-potery2" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent-potery2"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td class="columl1" ><?php print(t('Attic')); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc1_roof_space']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo-potery3" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent-potery3"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow last">
                            <td class="columl1" ><?php print(t('Windows doors')); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc1_window']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo-potery4" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent-potery4"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td colspan="3" class="pre-result"><?php print(t('Total:')); ?></td>
                            <td id="sum_result1" class="result sum1"><span class="value"></span></td>
                            <td class="result"><?php print(t('W/h')); ?></td>
                            <td id="sum_percent" class="result columl6"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td class="res-title"><?php print t('Total thermal resistance of the building:'); ?></td>
                            <td id="total_soprotivl"></td>
                            <td><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="result sum1"></td>
                            <td class="result"></td>
                            <td class="result columl6"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="post-value">
                <table>
                    <tbody>
                        <tr class="result">
                            <td rowspan="6" class="teplo"><?php print t('Heat losses<br /> in a value:'); ?></td>
                            <td class="date"><?php print t('In a day'); ?></td>
                            <td id="loss_heat_day1" class="value1"><span class="value"></span><?php print(t('kW/h')); ?></td>
                            <td id="loss_heat_cost_day1" class="value2"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td class="date"><?php print t('In a month'); ?></td>
                            <td id="loss_heat_month1"><span class="value"></span><?php print(t('kW/h')); ?></td>
                            <td id="loss_heat_cost_month1"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="electr"><?php print t('Electric heating'); ?></td>
                        </tr>
                        <tr>
                            <td class="date"><?php print t('In a day'); ?></td>
                            <td id="loss_gaz_heat_day1"><span class="value"></span><?php print(t('m')); ?><sup>3</sup></td>
                            <td id="loss_gaz_heat_cost_day1"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td class="date"><?php print t('In a month'); ?></td>
                            <td id="loss_gaz_heat_month1"><span class="value"></span><?php print(t('m')); ?><sup>3</sup></td>
                            <td id="loss_gaz_heat_cost_month1"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="gaz"><?php print t('Gas heating'); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="compare_2" class="compare_2">
        <?php print drupal_render($variables['form']['windows_type_2']); ?>
        <div class="value-box">
            <div class="enter-value">
                <table>
                            <tbody>
                        <tr class="t_header">
                            <th class="one" colspan="3"><?php print t('Normative value of heat transfer resistance<br />according to DBN В.2.6-31:2006'); ?></th>
                            <th class="two" colspan="3"><?php print t('Heat loss through the building structure, <span>* excluding ventilation losses and air-conditioning'); ?></span></th>
                        </tr>
                        <tr class="datarow">
                            <td class="columl1" ><?php print t('Walls'); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc2_walls']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo2-potery1" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent2-potery1"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td class="columl1" ><?php print t('Cellar'); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc2_basement']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo2-potery2" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent2-potery2"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td class="columl1" ><?php print t('Attic'); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc2_roof_space']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo2-potery3" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent2-potery3"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow last">
                            <td class="columl1" ><?php print t('Windows doors'); ?></td>
                            <td class="columl2"><?php print drupal_render($variables['form']['calc2_window']); ?></td>
                            <td class="columl3"><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="columl4" id="teplo2-potery4" ></td>
                            <td class="columl5"><?php print(t('W/h')); ?></td>
                            <td class="columl6" id="percent2-potery4"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td colspan="3" class="pre-result"><?php print t('Total:'); ?></td>
                            <td id="sum2_result1" class="result sum1"><span class="value"></span></td>
                            <td class="result"><?php print(t('W/h')); ?></td>
                            <td id="sum2_percent" class="result columl6"><span class="value"></span>%</td>
                        </tr>
                        <tr class="datarow">
                            <td class="res-title"><?php print t('Total thermal resistance of the building:'); ?></td>
                            <td id="total_soprotiv2"></td>
                            <td><?php print(t('m<sup>2</sup>К/W')); ?></td>
                            <td class="result sum1"></td>
                            <td class="result"></td>
                            <td class="result columl6"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="post-value">
                <table>
                    <tbody>
                        <tr class="result">
                            <td rowspan="6" class="teplo"><?php print t('Heat losses<br /> in a value:'); ?></td>
                            <td class="date"><?php print t('In a day'); ?></td>
                            <td id="loss_heat_day2" class="value1"><span class="value"></span><?php print(t('kW/h')); ?></td>
                            <td id="loss_heat_cost_day2" class="value2"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td class="date"><?php print t('In a month'); ?></td>
                            <td id="loss_heat_month2"><span class="value"></span><?php print(t('kW/h')); ?></td>
                            <td id="loss_heat_cost_month2"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="electr"><?php print t('Electric heating'); ?></td>
                        </tr>
                        <tr>
                            <td class="date"><?php print t('In a day'); ?></td>
                            <td id="loss_gaz_heat_day2"><span class="value"></span><?php print(t('m')); ?><sup>3</sup></td>
                            <td id="loss_gaz_heat_cost_day2"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td class="date"><?php print t('In a month'); ?></td>
                            <td id="loss_gaz_heat_month2"><span class="value"></span><?php print(t('m')); ?><sup>3</sup></td>
                            <td id="loss_gaz_heat_cost_month2"><span class="value"></span><?php print(t('uah')); ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" class="gaz"><?php print t('Gas heating'); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<h2 class="third title"><?php print t('Saving results with windows '); ?>"<span id="eco_product"></span>" <br /><?php print t('from '); ?>"Вікна ГВФ"</h2>
<div class="graph_region">
    <div class="graph">
        <div id="placeholder" class="graph_placeholder"></div>
        <h3><?php print t('Savings with windows from'); ?>“Вікна ГВФ”</h3>
        <ul>
            <li class="electr-line"><?php print t('Electric heating'); ?></li>
            <li class="gaz-line"><?php print t('Natural gas heating'); ?></li>
            <li class="comb-line"><?php print t('Combined heating'); ?><sup class="star">*</sup></li>
        </ul>
        <p><sup class="star">*</sup><?php print t('These data values calculated for combined heating are rough, and require conversion in each case.'); ?></p>
    </div>


    <div class="resilt">
        <table>
            <tbody>
                <tr>
                    <td class="header"><?php print t('Economy'); ?><sup class="star">*</sup> <br /><?php print t('in numbers'); ?><br /><?php print t('(Uah.)'); ?></td>
                    <td class="elektr"><img src="/sites/all/themes/gwf/images/elektr-otopl<?php print $language->language; ?>.png"></td>
                    <td class="gaz"><img src="/sites/all/themes/gwf/images/gaz-otopl<?php print $language->language; ?>.png"></td>
                    <td class="komb-otopl"><img src="/sites/all/themes/gwf/images/comb-otopl<?php print $language->language; ?>.png"></td>
                </tr>
                <tr class="pr_line">
                    <td></td>
                    <td id="elektr_ecomom_percent"></td>
                    <td id="gaz_ecomom_percent"></td>
                    <td id="comb_ecomom_percent"></td>
                </tr>
                <tr class="line">
                    <td><?php print t('1 month '); ?></td>
                    <td id="elektr_ecomom_month" class="eco_result"></td>
                    <td id="gaz_ecomom_month" class="eco_result"></td>
                    <td id="comb_ecomom_month" class="eco_result"></td>
                </tr>
                <tr class="line">
                    <td><?php print t('1 year '); ?></td>
                    <td id="elektr_ecomom_year" class="eco_result"></td>
                    <td id="gaz_ecomom_year" class="eco_result"></td>
                    <td id="comb_ecomom_year" class="eco_result"></td>
                </tr>
                <tr class="line">
                    <td ><?php print t('5 years'); ?></td>
                    <td id="elektr_ecomom_5years" class="eco_result"></td>
                    <td id="gaz_ecomom_5years" class="eco_result"></td>
                    <td id="comb_ecomom_5years" class="eco_result"></td>
                </tr>
                <tr  class="line">
                    <td ><?php print t('15 years'); ?></td>
                    <td id="elektr_ecomom_15years" class="eco_result"></td>
                    <td id="gaz_ecomom_15years" class="eco_result"></td>
                    <td id="comb_ecomom_15years" class="eco_result"></td>
                </tr>
                <tr  class="line">
                    <td ><?php print t('25 years'); ?></td>
                    <td id="elektr_ecomom_25years" class="eco_result"></td>
                    <td id="gaz_ecomom_25years" class="eco_result"></td>
                    <td id="comb_ecomom_25years" class="eco_result"></td>
                </tr>
            </tbody>
        </table>
        <p><sup class="star">*</sup><?php print t('Savings figures and these values may change with increasing tariffs rates for gas and electricity.'); ?></p>
    </div>
</div>