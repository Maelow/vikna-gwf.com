<?php
/**
 * @file
 * Zen theme's implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $secondary_menu_heading: The title of the menu used by the secondary links.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['navigation']: Items for the navigation region, below the main menu (if any).
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['footer']: Items for the footer region.
 * - $page['bottom']: Items to appear at the bottom of the page below the footer.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess_page()
 * @see template_process()
 */
?>

<div id="page">
<?php print render($page['language']); ?>
    <header id="header" role="banner">

        <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <hgroup id="header buttons">

        </hgroup><!-- /#name-and-slogan -->

        <?php print render($page['header']); ?>

    </header>

    <div id="navigation">
        <?php if ($main_menu): ?>
            <nav id="main-menu" role="navigation">
                <?php
                // This code snippet is hard to modify. We recommend turning off the
                // "Main menu" on your sub-theme's settings form, deleting this PHP
                // code block, and, instead, using the "Menu block" module.
                // @see http://drupal.org/project/menu_block
                print theme('links__system_main_menu', array(
                            'links' => $main_menu,
                            'attributes' => array(
                                'class' => array('links', 'inline', 'clearfix'),
                            ),
                            'heading' => array(
                                'text' => t('Main menu'),
                                'level' => 'h2',
                                'class' => array('element-invisible'),
                            ),
                        ));
                ?>
            </nav>
        <?php endif; ?>

        <?php print render($page['navigation']); ?>
    </div><!-- /#navigation -->

    <div id="image_slider">
        <div class="front-text">
        <h2><?php print t('17 years of progress'); ?></h2>
        <p><?php print t('Deep analysis, modern technologies <br />and promising trends are the future of our company!'); ?></p>
        </div>
        <div id="images-slides">
        <img src="/sites/all/themes/gwf/images/main3.jpg">
		<img src="/sites/all/themes/gwf/images/glass-3.jpg">
		<img src="/sites/all/themes/gwf/images/windows.jpg">
		<img src="/sites/all/themes/gwf/images/oknabig.jpg">
        </div>
        <div id="nav"></div>
    </div><!-- /#image slider -->
    <?php print render($page['pre_content']); ?>
    <div class="intro-text">
        <div class="econom-button"><?php print l(t('Economy calculator'), 'economy', array('attributes' => array('class' => array('calculation-econom'))))?></div>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <p><?php print t('During recent 17 years we achieved a considera­ble success. We were, we are and we will be one of a few those, who always do his/her job correctly and with high quality.'); ?></p>
    </div>
      <?php print render($tabs); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
    <div id="content" class="column" role="main">
    <div id="main">
    <?php print render($page['prod_content']); ?>
    <div class="container-wrap">
    <?php print render($page['content']); ?>
    <?php print render($page['post_content']); ?>
    </div>
    </div><!-- /#content -->

        <?php
        // Render the sidebars to see if there's anything in them.
        $sidebar_first = render($page['sidebar_first']);
        $sidebar_second = render($page['sidebar_second']);
        ?>

        <?php if ($sidebar_first || $sidebar_second): ?>
            <aside class="sidebars">
                <?php print $sidebar_first; ?>
                <?php print $sidebar_second; ?>
            </aside><!-- /.sidebars -->
        <?php endif; ?>
 </div><!-- /#main -->

    <?php print render($page['footer']); ?>
    <?php print render($page['postfooter']); ?>
</div><!-- /#page -->
<?php print $feed_icons; ?>
<?php print render($page['bottom']); ?>
